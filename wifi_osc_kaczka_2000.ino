/* Create a WiFi access point and provide a web server,
   Originally published by markingle on http://www.esp8266.com/viewtopic.php?p=47535
   Wersja bieżąca
*/
#include <FS.h>
#include <WebSocketsServer.h>
#include "HelperFunctions.h"



void setup() {
  pinMode(LED_BUILTIN, OUTPUT);     // Initialize the LED_BUILTIN pin as an output
  pinMode(D0, OUTPUT);
  pinMode(D1, OUTPUT);
  digitalWrite(D0, LOW);
  digitalWrite(D0, LOW);
  digitalWrite(LED_BUILTIN, HIGH);

  Serial.begin(115200);
  delay(1000);
  SPIFFS.begin();
  Serial.println(); Serial.print("Configuring access point...");
  setupWiFi();
  IPAddress myIP = WiFi.softAPIP();
  Serial.println("HTTP server started");
  server.on("/", HTTP_GET, []() {
    handleFileRead("/");
  });

  server.onNotFound([]() {                          // Handle when user requests a file that does not exist
    if (!handleFileRead(server.uri()))
      server.send(404, "text/plain", "FileNotFound");
  });

  webSocket.begin();                                // start webSocket server
  webSocket.onEvent(webSocketEvent);                // callback function
  server.begin();
  Serial.println("HTTP server started");
  delay(1000);
  yield();
  
  noInterrupts();
  // Timer 0
  timer0_isr_init();
  timer0_write(ESP.getCycleCount() + 160000000L); // 80MHz == 1sec
  
  // Timer 1
  timer1_isr_init();
  //timer1_attachInterrupt(sample_isr);
  timer1_enable(TIM_DIV16, TIM_EDGE, TIM_LOOP);
  timer1_write(clockCyclesPerMicrosecond() *16000); //80us = 12.5kHz sampling freq
  interrupts();

  delay(1000);
  yield();
}




//uint16_t i = 0;

void loop() {
  static unsigned long l = 0;                     // only initialized once
  unsigned long t;                                // local var: type declaration at compile time
  t = millis(); // returns number of miliseconds since the program started

  if ((t - l) > 100) {                           // update

    if (done == true) {
      writeToString();
      webSocket.sendTXT(socketNumber, "data," + strData);
      //--------------------------------------
      done = false;
      //--------------------------------------
    }

    //webSocket.sendTXT(socketNumber, "1,1," + String(arry[i]) + ",1");
    //i++;
    //if (i > 1500) i = 0;

    l = t;                                      // typical runtime this IF{} == 300uS - 776uS measured
    yield();
  }

  server.handleClient();
  webSocket.loop();
}
