document.getElementById("slidersample").addEventListener("slidersample", slidersample);
document.getElementById("sensp").addEventListener("redraw", redraw);
document.addEventListener("DOMContentLoaded", function(event) {
	console.log("DOM fully loaded and parsed");
});

var connection = new WebSocket('ws://' + location.hostname + ':81/', ['arduino']);
var msgArray;
var isBusy = 0;
var xax = 1; 
var channel = 1;
var continious = 0;
var sensRateVal = 1;
var jmpRateVal = 2;
var canvas = document.getElementById('myCanvas');
var context = canvas.getContext('2d');
var yVal;

context.lineWidth = 1;
context.lineJoin = 'round';
context.strokeStyle = '#1dd14f';
context.transform(1, 0, 0, -1, 0, 520);
context.beginPath();
		
connection.onopen = function () {
	connection.send('GET_TEMP');
};

console.log("connection opened");

connection.onerror = function (error) {
	console.log('WebSocket Error ', error);
};

function redraw() {
	context.closePath();
	context.clearRect(0, 0, canvas.width, canvas.height);
	context.beginPath();
	
	yVal = (msgArray[1]/2) * sensRateVal;
	if(yVal > 512) yVal = 512;
	context.moveTo(1, yVal);
	
	for(var i=2; i<500; i++) {	
		yVal = (msgArray[i]/2) * sensRateVal;
		if(yVal > 512) yVal = 512;
		context.lineTo(i*jmpRateVal, yVal);		
	}
	context.stroke();
}

connection.onmessage = function(evt) {
	console.log("dupa", sensRateVal);
	msgArray = evt.data.split(","); // split message by delimiter into a string array
	redraw();
	document.getElementById("starter").disabled = false; //enable starter button
	if(!continious)document.getElementById('starter').style.background='#0e822e';
	document.getElementById("saveButton").disabled = false; //enable starter button
	document.getElementById('saveButton').style.background='#0e822e';
	isBusy=0;
}

function showVal(value) {
	document.getElementById('sampleRate').innerHTML = value;
}

function showVal2(value) {
	jmpRateVal = value;
	document.getElementById('jmpRate').innerHTML = value;
	console.log(jmpRateVal);
	redraw();
}

function showVal3(value) {
	sensRateVal = value;
	document.getElementById('sensRate').innerHTML = value;
	console.log(sensRateVal);
	redraw();
}

function showVal4(value) {
	document.getElementById('triggerRate').innerHTML = value;
}	

function start() {
	if(!continious) {
		connection.send('#s'+ document.getElementById('sampleRate').innerHTML); // start
		document.getElementById("starter").disabled = true; //disable starter button
		document.getElementById('starter').style.background='#5e7768';
		console.log("presed");
		isBusy=1;
	}
}	

function continiousWork() {
	if(!isBusy) {
		var button = document.getElementById('continiousButton');		
		if(!continious) {
			connection.send('#c'+ document.getElementById('sampleRate').innerHTML); // start
			continious=1;
			document.getElementById('starter').disabled = true; //disable starter button_type2
			document.getElementById('continiousButton').style.background='#9e1116';
			document.getElementById('starter').style.background='#5e7768';
			button.title = "   on   ";
			button.innerHTML = "   on   ";		
		} else {
			connection.send('#p'+ document.getElementById('sampleRate').innerHTML); // start
			continious=0;
			document.getElementById('continiousButton').disabled = false; //disable starter button
			document.getElementById('starter').style.background='#0e822e';
			document.getElementById('continiousButton').style.background='#0e822e';
			button.title = "   off  ";
			button.innerHTML = "   off  ";
		}		

	}
}

function slidersample() {
	connection.send('#o'+ document.getElementById('sampleRate').innerHTML);
}

function ch1() {
	if(!isBusy) {
		channel=1;
		connection.send('#z'+ '0'); // start
		document.getElementById('ch1').style.background='#9e1116';
		document.getElementById('ch2').style.background='#0e822e';
		document.getElementById('ch3').style.background='#0e822e';
		document.getElementById('ch4').style.background='#0e822e';
		document.getElementById('all').style.background='#0e822e';
		console.log("sent new chanel 0");
	}
}

function ch2() {
	if(!isBusy) {
		channel=2;
		connection.send('#z'+ '1'); // start
		document.getElementById('ch1').style.background='#0e822e';
		document.getElementById('ch2').style.background='#9e1116';
		document.getElementById('ch3').style.background='#0e822e';
		document.getElementById('ch4').style.background='#0e822e';
		document.getElementById('all').style.background='#0e822e';
		console.log("sent new chanel 1");
	}
}

function ch3() {
	if(!isBusy) {
		channel=3;
		connection.send('#z'+ '2'); // start
		document.getElementById('ch1').style.background='#0e822e';
		document.getElementById('ch2').style.background='#0e822e';
		document.getElementById('ch3').style.background='#9e1116';
		document.getElementById('ch4').style.background='#0e822e';
		document.getElementById('all').style.background='#0e822e';
		console.log("sent new chanel 2");
	}
}

function ch4() {
	if(!isBusy) {
		channel=4;
		connection.send('#z'+ '3'); // start
		document.getElementById('ch1').style.background='#0e822e';
		document.getElementById('ch2').style.background='#0e822e';
		document.getElementById('ch3').style.background='#0e822e';
		document.getElementById('ch4').style.background='#9e1116';
		document.getElementById('all').style.background='#0e822e';
		console.log("sent new chanel 3");
	}
}

function allsc() {
	if(!isBusy) {
		channel=011;
		connection.send('#z'+ '4'); // start
		document.getElementById('ch1').style.background='#9e1116';
		document.getElementById('ch2').style.background='#9e1116';
		document.getElementById('ch3').style.background='#9e1116';
		document.getElementById('ch4').style.background='#9e1116';
		console.log("sent all chanels");
	}
}

function saveToFile() {
	isBusy = 1;
	var sampleTime = (1 / document.getElementById('sampleRate').innerHTML)*1000;
	var content = "U[mV] t[us]\n";			
	
	for(var i=2; i<500; i++) {
		content += Math.round((msgArray[i]*33)/10);
		content += ' ';
		content += Math.round(i*sampleTime*1000000);
		content += '\n';
	}
	
	var file = new File([content], "Signal.txt", {type: "text/plain;charset=utf-8"});
	saveAs(file);
	isBusy = 0;
}	

function saveToPng() {
	var d = new Date();
	context.font = "20px Arial";
	context.fillText(d.toLocaleString(),10,30);
	context.fillText("Sample rate: " + document.getElementById('sampleRate').innerHTML + " Channel: " + channel,10,60); 	
	context.strokeRect(0, 0, canvas.width, canvas.height);
	context.stroke();
	canvas.toBlob(function(blob) {
		saveAs(blob, "signal_image.png");
	});
	redraw();
}