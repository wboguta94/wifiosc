/* Go to http:// 192.168.4.1 in a web browser
   connected to this access point to see it.
*/

#include <ESP8266WebServer.h>
#include <WebSocketsServer.h>

void setSource(void);
void writeToString(void);

uint8_t            socketNumber;
char               AP_NameChar[6];            // AP_NameString.length() + 1];
const char         WiFiAPPSK[]   = "dupa";        // "put your password here before compiling" - creative one recomended xD
String             AP_NameString = "Kaczka2000";
String             temp_str;
String             tmp;

ESP8266WebServer server(80);
WebSocketsServer webSocket(81);               // Create a Websocket server

void handleRoot() {
  server.send(200, "text/html", "<h1>You are connected</h1>");
}

uint16_t arry[1500];
uint32_t sampleRate;
uint32_t timer0Register = 160000000L;
bool toggle = false;
volatile uint16_t cnt = 0;
String strData;
bool done = false;
int chooseSource = 0;
bool continous = 0;

void timer0_ISR1 (void) {
  arry[cnt] = analogRead(A0);
  cnt++;

  if (cnt >= 500) {
    cnt = 0;
    done = true;
    timer0_detachInterrupt();
  }

  timer0_write(ESP.getCycleCount() + timer0Register); // 160MHz == 1sec
}


void sample_isr(void)
{
  /*  if (toggle) {
      digitalWrite(LED_BUILTIN, HIGH); toggle = false;
    } else {
      digitalWrite(LED_BUILTIN, LOW); toggle = true;
    }*/
  timer0_attachInterrupt(timer0_ISR1);
  strData = "";
}


void webSocketEvent(uint8_t num, WStype_t type, uint8_t * payload, size_t lenght) {

  switch (type) {
    case WStype_DISCONNECTED:
      // Reset the control for sending samples of ADC to idle to allow for web server to respond.
      Serial.printf("[%u] Disconnected!\n", num);
      yield();
      break;

    case WStype_CONNECTED: {                  // Braces required http://stackoverflow.com/questions/5685471/error-jump-to-case-label
        IPAddress ip = webSocket.remoteIP(num);
        Serial.printf("[%u] Connected from %d.%d.%d.%d url: %s\n", num, ip[0], ip[1], ip[2], ip[3], payload);
        yield();
        socketNumber = num;
        break;
      }

    case WStype_TEXT:
      if (payload[0] == '#') {
        Serial.printf("[%u] get Text: %s\n", num, payload);

        if (payload[1] == 's') {
          tmp = "";
          for (int i = 2; isDigit(payload[i]); i++) {
            tmp += (char)payload[i];
          }
          sampleRate = tmp.toInt();
          timer0Register = 160000000L / sampleRate;
          cnt = 0;
          timer0_attachInterrupt(timer0_ISR1);
          strData = "";
          Serial.printf("Measure, sample rate: %d\n", sampleRate);
        }

        if (payload[1] == 'c') {
          strData = "";
          tmp = "";
          for (int i = 2; isDigit(payload[i]); i++) {
            tmp += (char)payload[i];
          }
          sampleRate = tmp.toInt();
          timer0Register = 160000000L / sampleRate;
          cnt = 0;
          noInterrupts();
          //timer1_isr_init();
          //timer1_attachInterrupt(sample_isr);
          
          continous = 1;
          timer0_attachInterrupt(timer0_ISR1);
          
          //timer1_enable(TIM_DIV16, TIM_EDGE, TIM_LOOP);
          //timer1_write(clockCyclesPerMicrosecond() * 10000); //80us = 12.5kHz sampling freq
          interrupts();
          Serial.printf("Continous, sample rate: %d\n", sampleRate);
        }

        if (payload[1] == 'p') {
          cnt = 0;
          tmp = "";
          strData = "";
          //timer1_detachInterrupt();
          continous = 0;
          Serial.printf("Continous stop");
        }

        if (payload[1] == 'o') {
          cnt = 0;
          tmp = "";
          strData = "";
          for (int i = 2; isDigit(payload[i]); i++) {
            tmp += (char)payload[i];
          }
          sampleRate = tmp.toInt();
          timer0Register = 160000000L / sampleRate;
          Serial.printf(" sample rate set: %d\n", sampleRate);
        }
        
        if (payload[1] == 'z') {
          tmp = "";
          for (int i = 2; isDigit(payload[i]); i++) {
            tmp += (char)payload[i];
          }
          chooseSource = tmp.toInt();
          setSource();
          Serial.printf("Source set\n");
        }

        yield();
      }
      break;

    case WStype_ERROR:
      Serial.printf("Error [%u] , %s\n", num, payload);
      yield();
  }
}


String getContentType(String filename) {
  yield();
  if (server.hasArg("download"))      return "application/octet-stream";
  else if (filename.endsWith(".htm")) return "text/html";
  else if (filename.endsWith(".html"))return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".js"))  return "application/javascript";
  else if (filename.endsWith(".png")) return "image/png";
  else if (filename.endsWith(".gif")) return "image/gif";
  else if (filename.endsWith(".jpg")) return "image/jpeg";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if (filename.endsWith(".xml")) return "text/xml";
  else if (filename.endsWith(".pdf")) return "application/x-pdf";
  else if (filename.endsWith(".zip")) return "application/x-zip";
  else if (filename.endsWith(".gz"))  return "application/x-gzip";
  else if (filename.endsWith(".svg")) return "image/svg+xml";
  return "text/plain";
}

bool handleFileRead(String path) {
  if (path.endsWith("/")) {
    path += "counter.html";
  }
  String contentType = getContentType(path);
  String pathWithGz = path + ".gz";
  if (SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)) {
    if (SPIFFS.exists(pathWithGz)) path += ".gz";
    File file = SPIFFS.open(path, "r");
    size_t sent = server.streamFile(file, contentType);
    file.close();
    return true;
  }
  yield();
  return false;
}

void setupWiFi() {
  WiFi.mode(WIFI_AP);
  // char AP_NameChar[AP_NameString.length() + 1];
  memset(AP_NameChar, 0, AP_NameString.length() + 1);
  for (int i = 0; i < AP_NameString.length(); i++)
    AP_NameChar[i] = AP_NameString.charAt(i);
  yield();
  WiFi.softAP(AP_NameChar, WiFiAPPSK);
}
//----------------------------------------------------

void writeToString(void) {
  for (int i = 0; i < 500; i++) {
    strData += String(arry[i]) + ',';
  }
}

void setSource()
{
  switch (chooseSource) {
    case 0:
      Serial.printf("default source NR 0\n");
      digitalWrite(D0, LOW);
      digitalWrite(D1, LOW);
      break;
    case 1:
      Serial.printf("source NR 1 \n");
      digitalWrite(D0, HIGH);
      digitalWrite(D1, LOW);
      break;
    case 2:
      Serial.printf(" source NR 2\n");
      digitalWrite(D0, LOW);
      digitalWrite(D1, HIGH);
      break;
    case 3:
      Serial.printf("[%u] Source nr 3\n");
      digitalWrite(D0, HIGH);
      digitalWrite(D1, HIGH);
      break;
    default:
      Serial.printf(" Unknown Source\n");
      break;
  }
}
